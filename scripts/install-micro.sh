#!/bin/bash
mkdir -p $HOME/bin
wget https://github.com/zyedidia/micro/releases/download/nightly/micro-1.3.5-17-linux64.tar.gz -O /tmp/micro.tar.gz 
tar xf /tmp/micro.tar.gz micro-1.3.5-17/micro -O > $HOME/bin/micro
rm /tmp/micro.tar.gz
chmod a+x $HOME/bin/micro
